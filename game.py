from random import randint
# ask for a name

name = input('Hi! What is your name? ')
for guess_number in range(1, 6):
    month_number = randint(1, 12)
    year_number = randint(1924, 2004)

    print(f"Guess {guess_number}: were you born in {month_number}/{year_number}?")

    response = input('Yes or No? ')

    if response == 'yes':
        print('I knew it!')
        exit()
    elif guess_number == 5:
        print("I have other things to do. Good bye!")
    else:
        print('Drat! Lemme try again!')
